# LexLR

LexLR is a rust library for creating parsers of regular and context free LALR(1) languages.

## Setup

Download rust [here](https://www.rust-lang.org/learn/get-started) if you don't already have it,
then clone this repo. You can then add it as a dependency of any other project on your computer by
listing it as a local dependency, which can be done by adding something like this in your
`cargo.toml`:

```
[dependencies]
my_lib = { path = "../lexlr" }
```

## Docs

The documentation is available [here](https://lionelf329.gitlab.io/lexlr/lexlr).

## What it does

Most programming languages are parsed in two steps - the characters in the source are first grouped
into tokens, and then a tree-like hierarchy is found in that list according to the syntax of the
language. This library takes as input a list of regular expressions and literal strings to use for
tokenization, and a context free grammar for generating the parse tree. It then goes through several
steps as described below.

#### Part 1: Regular languages

1. Split the regular expressions into tokens. For example, the regex `"a\+a"` represents the
   literal string `"a+a"`, so it would be tokenized as `["a", "+", "a"]`.
2. Parse the stream of tokens to interpret the syntax. For example, the regex `"[ab]c"` means
   `('a' or 'b') followed by 'c'`. Parsing it gives the order of operations, which prepares it
   for the next step.
3. Create a non-deterministic finite state machine (ndfsm) from the set of literals and regular
   expression parse trees. This uses a process similar to
   [Thompson's construction](https://en.wikipedia.org/wiki/Thompson%27s_construction), although it
   has been modified to create fewer unnecessary states.
4. Convert the dfsm to a deterministic finite state machine (dfsm) using an algorithm that treats
   each subset of states in the ndfsm as one state in the dfsm. This means the dfsm could
   hypothetically have as many as 2^n states as compared to the n states in the ndfsm, however in
   practice it is much, much lower. This step drastically reduces the runtime when reading input,
   because the process of simulating a dfsm is significantly cheaper than simulating an ndfsm.
5. Minimize the dfsm by merging states that are equivalent. In broad terms, this is done by
   merging all the states, and then progressively splitting them based on whether they behave
   differently on the same input. This step should have very little impact on speed at runtime in
   most cases aside from cache locality, however it reduces the number of states, which means the
   tables will take up less space.

#### Part 2: Context-free languages

1. The grammar allows you to create terminals with generics, so the first step is to traverse the
   grammar to find all the ways that each nonterminal is used. This stage also involves adding a
   new nonterminal to act as the root with one production: `S' -> S EOF`. This is needed in order
   to generate the parse tables, because the LALR algorithm must always have a token that it can
   look ahead to.
2. Determine whether each nonterminal can be matched by epsilon (the empty string). This is done
   based on three assumptions:
   
   1. Terminals can never be matched by epsilon
   2. If every rule belonging to a nonterminal has at least one element that can't be matched by
      epsilon, then that nonterminal can't be matched by epsilon
   3. If some rule belonging to a nonterminal does not have any elements that can't be matched by
      epsilon, then that nonterminal can be matched by epsilon
   
   These three assumptions collectively are sufficient to determine the answer for every
   nonterminal in the grammar, just by applying 2 and 3 over and over to all the symbols. If there
   is an infinite loop, that means that the grammar has a loop within itself, which makes the
   language invalid, and the question unanswerable. This can be easily detected by making sure
   that each iteration brings it closer to completion.
3. Find all the terminals that can appear at the beginning of each nonterminal.
4. Create the two parse tables, `ACTION` and `GOTO`. This step is brutally complicated.

#### Part 3: Using generated tables

After the above steps finish, three tables will have been calculated that can be used to parse
input in the defined language. This leaves two final algorithms to implement - one to use the
minimal dfsm to convert a character stream into a token stream, and one to use the action and goto
tables to convert a list of tokens into a parse tree.
