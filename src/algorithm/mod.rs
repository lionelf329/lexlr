use crate::Action;
use tokenizer::Tokenizer;

pub use parse_tree::ParseTree;
pub use parse_wrapper::ParseWrapper;
pub use parser_data::{OwnedParserData, ParserData, StaticParserData};
pub use text_range::R;

mod parse_tree;
mod parse_wrapper;
mod parser_data;
mod text_range;
mod tokenizer;

/// Shorthand for popping several enum values from a vector and unwrapping their inner values.
#[macro_export]
macro_rules! pop_many {
    ($s:expr, $($variants:ident),*) => {
        (
            $(match $s.pop() {
                Some($variants(x)) => x,
                _ => unreachable!(),
            }),*,
        )
    };
}

/// Defines the reducers used in an LALR parser.
pub trait SymbolExt<Root, State>: Sized {
    /// Applies the specified nonterminal reduction on the provided stacks.
    fn reduce(state: &mut State, s1: &mut Vec<Self>, s2: &mut Vec<usize>, nt: usize, rule: usize);

    /// Applies the specified regular expression reduction on the provided stack.
    fn reduce_regexp(
        state: &mut State,
        s1: &mut Vec<Self>,
        mode: usize,
        id: usize,
        text: String,
        range: R,
    );

    /// Returns an `Option<T>` containing the start symbol, if `self` is the correct variant.
    fn get_s(self) -> Option<Root>;
}

/// Enables parsing of an iterator of characters using parse tables.
pub trait Parse {
    /// Parse `self` using a specified parser.
    fn parse<Symbol: SymbolExt<Root, State>, Root, State, Data: ParserData<Symbol>>(
        self,
        global_state: &mut State,
        data: &Data,
    ) -> Result<Root, R>;
}

impl<Iter: Iterator<Item = char>> Parse for Iter {
    fn parse<Symbol: SymbolExt<Root, State>, Root, State, Data: ParserData<Symbol>>(
        self,
        global_state: &mut State,
        data: &Data,
    ) -> Result<Root, R> {
        let mut lexer = Tokenizer::new(self, data);

        // Initialize the state of the parser with an empty parse, and a state of 0
        let mut stack1: Vec<Symbol> = Vec::new();
        let mut stack2: Vec<usize> = vec![0];
        let mut lookahead = lexer.get_token(data.lexer_mode(0));
        let mut last_range = (0..1).into();

        // Main parse loop
        loop {
            // Get the state at the top of stack
            let state = *stack2.last().unwrap();
            let index = lookahead.as_ref().map(|x| x.0).unwrap_or(0);

            match data.get_action(state, index) {
                Action::Accept => return Ok(stack1.pop().unwrap().get_s().unwrap()),
                Action::Shift(n) => {
                    if let Some((id, range, string)) = lookahead {
                        last_range = range;
                        Symbol::reduce_regexp(
                            global_state,
                            &mut stack1,
                            data.lexer_mode(state),
                            id,
                            string,
                            range,
                        );
                    }
                    stack2.push(n);
                    lookahead = lexer.get_token(data.lexer_mode(n));
                }
                Action::Reduce(n1, n2) => {
                    Symbol::reduce(global_state, &mut stack1, &mut stack2, n1, n2);
                    stack2.push(data.get_goto(*stack2.last().unwrap(), n1));
                }
                Action::Error => return Err(lookahead.map_or(last_range, |x| x.1)),
            }
        }
    }
}
