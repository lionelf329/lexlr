use super::parse_tree::ParseTreeState;
use crate::{OwnedParserData, ParseTree, R};

/// Allows a `TokenStream<'a, T>` to be constructed from a character iterator.
pub trait ParseWrapper<T: Iterator<Item = char>> {
    /// Uses lalr tables to create a parse tree for a given input token stream.
    fn parse(self, data: &OwnedParserData) -> Result<ParseTree, R>;
}

impl<T: Iterator<Item = char>> ParseWrapper<T> for T {
    fn parse(self, data: &OwnedParserData) -> Result<ParseTree, R> {
        let mut state = ParseTreeState {
            language: data.language,
            parser: &data.parser,
        };
        super::Parse::parse(self, &mut state, data)
    }
}
