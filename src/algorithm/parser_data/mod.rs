use crate::{Action, Token};

pub use owned_parser_data::OwnedParserData;
pub use static_parser_data::StaticParserData;

mod owned_parser_data;
mod static_parser_data;

/// A trait for types that are able to act as lexer tables and parse tables.
pub trait ParserData<Symbol> {
    /// Returns the next state for the given lexer mode, state, and character.
    fn lexer_transition(&self, mode: usize, state: usize, char: char) -> Option<usize>;

    /// Returns the label for the lexer mode and state.
    fn lexer_label(&self, mode: usize, state: usize) -> Option<(Token, Option<usize>)>;

    /// Returns the lexer mode for the parser state.
    fn lexer_mode(&self, state: usize) -> usize;

    /// Returns the action for the given parser state and token index.
    fn get_action(&self, state: usize, index: usize) -> Action;

    /// Returns the next state for the given parser state and nonterminal index.
    fn get_goto(&self, state: usize, index: usize) -> usize;
}
