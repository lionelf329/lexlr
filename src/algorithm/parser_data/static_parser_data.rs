use crate::{char_to_index, Action, ParserData, Token, ALPHABET_SIZE};
use std::marker::PhantomData;

/// Contains all the data needed to use the generated DFA and LALR tables.
#[derive(Debug)]
pub struct StaticParserData<Symbol> {
    /// A blob of data containing lexer transitions for each pair of state and character.
    lexer_tables: &'static [&'static [[usize; ALPHABET_SIZE]]],
    /// A list of labels for the lexer states.
    lexer_labels: &'static [&'static [Option<(Token, Option<usize>)>]],
    /// A list of parser states, with lexer modes and action lists.
    parser_table: &'static [(usize, &'static [Action])],
    /// A function that maps parser state and nonterminal to a new parser state.
    goto: fn(usize, usize) -> usize,
    /// A marker indicating which nonterminal to parse with.
    phantom_data: PhantomData<Symbol>,
}

impl<Symbol> StaticParserData<Symbol> {
    /// Constructs a `ParserData` object.
    pub const fn new(
        lexer_tables: &'static [&'static [[usize; ALPHABET_SIZE]]],
        lexer_labels: &'static [&'static [Option<(Token, Option<usize>)>]],
        parser_table: &'static [(usize, &'static [Action])],
        goto: fn(usize, usize) -> usize,
    ) -> Self {
        Self {
            lexer_tables,
            lexer_labels,
            parser_table,
            goto,
            phantom_data: PhantomData,
        }
    }
}

impl<Symbol> ParserData<Symbol> for StaticParserData<Symbol> {
    fn lexer_transition(&self, mode: usize, state: usize, char: char) -> Option<usize> {
        match self.lexer_tables[mode][state][char_to_index(char)] {
            0 => None,
            n => Some(n - 1),
        }
    }

    fn lexer_label(&self, mode: usize, state: usize) -> Option<(Token, Option<usize>)> {
        self.lexer_labels[mode][state]
    }

    fn lexer_mode(&self, state: usize) -> usize {
        self.parser_table[state].0
    }

    fn get_action(&self, state: usize, index: usize) -> Action {
        self.parser_table[state].1[index]
    }

    fn get_goto(&self, state: usize, index: usize) -> usize {
        (self.goto)(state, index)
    }
}
