use crate::{ParserData, R};
use std::collections::VecDeque;
use std::marker::PhantomData;

pub struct Tokenizer<'a, Iter: Iterator<Item = char>, Symbol, Data: ParserData<Symbol>> {
    /// The data providing lexer transitions and lexer state labels.
    data: &'a Data,
    /// The character stream being parsed.
    iter: Iter,
    /// The number of characters in previous tokens.
    index: usize,
    /// A buffer for characters that haven't been emitted as tokens yet.
    buffer: VecDeque<char>,
    /// A marker indicating which nonterminal to parse with.
    phantom_data: PhantomData<Symbol>,
}

impl<'a, Iter: Iterator<Item = char>, Symbol, Data: ParserData<Symbol>>
    Tokenizer<'a, Iter, Symbol, Data>
{
    /// Creates a new [Tokenizer] from a character iterator and a [ParserData] implementation.
    pub fn new(iter: Iter, data: &'a Data) -> Self {
        Self {
            data,
            iter,
            index: 0,
            buffer: VecDeque::new(),
            phantom_data: PhantomData,
        }
    }

    /// Gets the next non-ignored match.
    pub fn get_token(&mut self, mode: usize) -> Option<(usize, R, String)> {
        loop {
            match self.get_next_match(mode) {
                Some(Ok((Some(label), range, str))) => return Some((label, range, str)),
                Some(Err(x)) => panic!("Lexer error: {:?}", x),
                None => return None,
                Some(Ok((None, _, _))) => {}
            }
        }
    }

    /// Gets the next token that matches regardless of whether it will be ignored by the parser,
    /// returning an error at the first invalid character if no token matches.
    fn get_next_match(
        &mut self,
        mode: usize,
    ) -> Option<Result<(Option<usize>, R, String), (R, char)>> {
        let mut len = 0;
        let mut state = 0;
        let mut last_acceptance = self.current_match(mode, state, len);
        while let Some(c) = self.take_char(len) {
            len += 1;
            if self.transition(c, mode, &mut state, len, &mut last_acceptance) {
                break;
            }
        }
        self.return_val(last_acceptance)
    }

    /// Gets the character at the specified index from the unconsumed input.
    fn take_char(&mut self, i: usize) -> Option<char> {
        if i < self.buffer.len() {
            Some(self.buffer[i])
        } else if let Some(c) = self.iter.next() {
            assert_eq!(i, self.buffer.len());
            self.buffer.push_back(c);
            Some(c)
        } else {
            None
        }
    }

    /// Takes the matched characters out of the buffer, updates the counter for consumed
    /// characters, and returns a value representing the result of the parse operation.
    fn return_val(
        &mut self,
        last_acceptance: Option<(usize, Option<usize>)>,
    ) -> Option<Result<(Option<usize>, R, String), (R, char)>> {
        if let Some((len, label)) = last_acceptance {
            let str = self.buffer.drain(0..len).collect::<String>();
            Some(Ok((label, self.range(str.len()), str)))
        } else if let Some(c) = self.take_char(0) {
            self.buffer.pop_front();
            Some(Err((self.range(c.len_utf8()), c)))
        } else {
            None
        }
    }

    /// Shifts the index forward by the specified amount and returns the range in between.
    fn range(&mut self, len: usize) -> R {
        let start = self.index;
        self.index += len;
        (start..self.index).into()
    }

    /// Follows a transition in the lexer, updating the state and last accepted match.
    /// Returns true if the new state is dead.
    fn transition(
        &self,
        c: char,
        mode: usize,
        state: &mut usize,
        len: usize,
        last_acceptance: &mut Option<(usize, Option<usize>)>,
    ) -> bool {
        if let Some(new_state) = self.data.lexer_transition(mode, *state, c) {
            *state = new_state;
            *last_acceptance = self.current_match(mode, *state, len).or(*last_acceptance);
            false
        } else {
            true
        }
    }

    /// Returns the token length and type if the current state has a label.
    fn current_match(
        &self,
        mode: usize,
        state: usize,
        len: usize,
    ) -> Option<(usize, Option<usize>)> {
        self.data.lexer_label(mode, state).map(|(_, s)| (len, s))
    }
}
