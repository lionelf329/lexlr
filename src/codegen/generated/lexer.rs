use crate::{Lexer, Token};

impl Lexer {
    pub(super) fn fmt_states_and_labels(&self, mapping: &[Vec<Token>]) -> (String, String) {
        let mut states = String::new();
        let mut labels = String::new();
        let (start_automata, indent_str, end_automata, end_list) = match self.automata.len() {
            1 => ("&[", "", "\n]", ""),
            _ => ("\n    &[", "    ", "\n    ],", "\n"),
        };
        for (i, automata) in self.automata.iter().enumerate() {
            states.push_str(start_automata);
            labels.push_str(start_automata);
            for (label, state) in &automata.states {
                states.push_str("\n    ");
                states.push_str(indent_str);
                states.push_str("[\n       ");
                states.push_str(indent_str);
                let mut indent = 7 + indent_str.len();
                for n in state {
                    let s = format!(" {},", n.map_or(0, |n| n + 1));
                    if indent + s.len() > 99 {
                        states.push_str("\n       ");
                        states.push_str(indent_str);
                        indent = 7 + indent_str.len();
                    }
                    indent += s.len();
                    states.push_str(&s);
                }
                labels.push_str(
                    &format!(
                        "\n    {}{:?},",
                        indent_str,
                        label.map(|automata_token| (
                            automata_token,
                            mapping[i]
                                .iter()
                                .position(|&mapping_token| mapping_token == automata_token)
                        )),
                    )
                    .replace("String", "Token::String")
                    .replace("Regexp", "Token::Regexp"),
                );
                states.push_str("\n    ");
                states.push_str(indent_str);
                states.push_str("],");
            }
            states.push_str(end_automata);
            labels.push_str(end_automata);
        }
        states.push_str(end_list);
        labels.push_str(end_list);
        (states, labels)
    }
}
