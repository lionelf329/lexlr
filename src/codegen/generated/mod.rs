use crate::{OwnedParserData, SymbolID, Token};

mod lexer;
mod parser;

pub fn generated(data: &OwnedParserData, crate_name: &str) -> String {
    let mut enum_lines = String::new();
    let mut match_branches_regex = String::new();
    for (i, j, r) in data.language.regexps() {
        let k = data.mapping[i]
            .iter()
            .position(|&t| t == Token::Regexp(i, j))
            .unwrap();
        enum_lines.push_str(&format!(
            "\n    /// `{}`\n    T{}_{}({}),",
            r.text, i, j, r.name
        ));
        match_branches_regex.push_str(&format!(
            "\n            ({}, {}) => s1.push(T{}_{}(regex{}_{}(s, range, text))),",
            i, k, i, j, i, j
        ));
    }

    let mut match_branches_nt = String::new();
    for (i, nt) in data.parser.nonterminals.iter().enumerate().skip(1) {
        enum_lines.push_str(&format!(
            "\n    /// {}\n    S{}({}),",
            nt.name.describe(&[], data.language),
            i,
            nt.name.type_str(&[], data.language)
        ));
        let nt_original = nt.name.name.symbol().unwrap();
        for (&j, r) in nt.rules.iter() {
            let original_rule = &data.language.symbols[nt_original].rules[&j];
            let mut method = Vec::new();
            if !r.is_empty() {
                method.push(format!("s2.truncate(s2.len() - {});", r.len()));
            }
            let mut params: Vec<String> = Vec::new();
            for (e, _e2) in r.iter().zip(original_rule.iter()) {
                match e {
                    &SymbolID::T(Token::EOF) => {}
                    &SymbolID::T(Token::String(_, _)) => {
                        params.push(String::from("T"));
                    }
                    &SymbolID::T(Token::Regexp(i, j)) => {
                        params.push(format!("T{}_{}", i, j));
                    }
                    &SymbolID::NT(i) => {
                        params.push(format!("S{}", i));
                    }
                }
            }
            params.reverse();
            if !params.is_empty() {
                method.push(format!("let x = pop_many!(s1, {});", params.join(", ")));
            }
            let args = (0..params.len())
                .rev()
                .map(|i| format!(", x.{}", i))
                .collect::<String>();
            method.push(format!(
                "s1.push(S{}(rule{}_{}(s{})));",
                i, nt_original, j, args
            ));
            method
                .iter_mut()
                .for_each(|x| *x = format!("                {}", x));
            let method = method.join("\n");
            match_branches_nt.push_str(&format!(
                "\n            ({}, {}) => {{\n{}\n            }}",
                i, j, method
            ));
        }
    }

    let (states, labels) = data.lexer.fmt_states_and_labels(&data.mapping);
    let (action, goto) = data.parser.fmt_action_and_goto(&data.mapping);
    let constants = format!(
        include_str!("enum_template.txt"),
        crate_name,
        data.language.vocabularies.len(),
        states,
        data.language.vocabularies.len(),
        labels,
        action,
        goto,
        enum_lines,
        match_branches_nt,
        match_branches_regex,
    );
    constants
}
