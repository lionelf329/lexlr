use crate::{Parser, Token};

impl Parser<'_> {
    pub(super) fn fmt_action_and_goto(&self, mapping: &[Vec<Token>]) -> (String, String) {
        let mut action_str = String::new();
        let mut goto_str = String::new();
        for (i, s) in self.states.iter().enumerate() {
            let lexer = s.lexer.unwrap_or(0);
            action_str.push_str(&format!(
                "\n    (\n        {},\n        &[",
                s.lexer.unwrap_or(0)
            ));
            for action in mapping[lexer].iter().map(|&t| s.action(t)) {
                action_str.push_str(&format!("\n            {:?},", action));
            }
            action_str.push_str("\n        ],\n    ),");
            for (j, &goto) in s.goto.iter().enumerate() {
                if let Some(goto) = goto {
                    goto_str.push_str(&format!("\n        ({}, {}) => {},", i, j, goto));
                }
            }
        }
        (action_str, goto_str)
    }
}
