use crate::{Language, OwnedParserData, SymbolRef, Terminal};
use std::fs::File;
use std::io::Write;

mod generated;
mod reducers;

/// Generates a collection of rust source files that parse the given language.
pub fn write(data: &OwnedParserData, path: &str, is_self: bool) {
    let crate_name = match is_self {
        true => "crate",
        false => "lexlr",
    };
    let generated = generated::generated(data, crate_name);
    let reducers = reducers::reducers(data, crate_name, path);
    write_file(&format!("{}/generated.rs", path), generated.as_bytes());
    write_file(&format!("{}/reducers.rs", path), reducers.as_bytes());
}

fn write_file(path: &str, buf: &[u8]) {
    File::create(path).unwrap().write(buf).unwrap();
}

impl Language<SymbolRef> {
    fn regexps(&self) -> impl Iterator<Item = (usize, usize, &Terminal)> {
        self.vocabularies.iter().enumerate().flat_map(|(i, v)| {
            v.regexps
                .iter()
                .enumerate()
                .filter(|(_, x)| !x.ignored)
                .map(move |(j, r)| (i, j, r))
        })
    }
}
