use std::fmt::{Display, Formatter};

/// A function for a grammar rule or regex that builds an AST node.
pub struct Reducer {
    /// A description of the grammar rule or regex this reducer is used for.
    pub comment: String,
    /// An identifier of the grammar rule or regex this reducer is used for.
    pub reducer_type: ReducerType,
    /// The list of generic parameters for the nonterminal.
    pub generics: Vec<String>,
    /// A list of names and types for the function's parameters.
    pub params: Vec<(String, String)>,
    /// The name of the function's return type.
    pub return_type: String,
    /// The body of the function, either auto-generated or scraped from the existing file.
    pub body: String,
}

impl Display for Reducer {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "// {}\npub fn {}", self.comment, self.reducer_type)?;
        if !self.generics.is_empty() {
            f.write_str("<")?;
            for (i, name) in self.generics.iter().enumerate() {
                if i != 0 {
                    f.write_str(", ")?;
                }
                f.write_str(name)?;
            }
            f.write_str(">")?;
        }
        f.write_str("(_: &mut State")?;
        for (name, arg_type) in self.params.iter() {
            f.write_str(", ")?;
            write!(f, "{}: {}", name, arg_type)?;
        }
        write!(f, ") -> {} {{\n{}\n}}", self.return_type, self.body)
    }
}

pub enum ReducerType {
    Regexp(usize, usize),
    Symbol(usize, usize),
}

impl Display for ReducerType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ReducerType::Regexp(a, b) => write!(f, "regex{}_{}", a, b),
            ReducerType::Symbol(a, b) => write!(f, "rule{}_{}", a, b),
        }
    }
}
