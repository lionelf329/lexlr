use super::reducers::*;
use crate::{pop_many, Action, Action::*, StaticParserData, SymbolExt, Token, ALPHABET_SIZE, R};
use Symbol::*;

pub const DATA: StaticParserData<Symbol> = StaticParserData::new(&LEXER, &LABELS, &STATES, goto);

const LEXER: [&[[usize; ALPHABET_SIZE]]; 2] = [
    &[
        [
            0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 3, 4, 5, 6, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
            2, 2, 2, 2, 2, 7, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
            2, 2, 2, 2, 8, 9, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
            2, 2, 2, 2, 2, 2, 2, 2, 10, 2, 2, 0,
        ],
        [
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        ],
        [
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        ],
        [
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        ],
        [
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        ],
        [
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        ],
        [
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        ],
        [
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        ],
        [
            0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0,
        ],
        [
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        ],
        [
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        ],
    ],
    &[
        [
            0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
            2, 2, 2, 2, 2, 4, 5, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0,
        ],
        [
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        ],
        [
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        ],
        [
            0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
            2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0,
        ],
        [
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        ],
    ],
];

const LABELS: [&[Option<(Token, Option<usize>)>]; 2] = [
    &[
        None,
        Some((Token::Regexp(0, 0), Some(9))),
        Some((Token::String(0, 0), Some(1))),
        Some((Token::String(0, 1), Some(2))),
        Some((Token::String(0, 4), Some(5))),
        Some((Token::String(0, 5), Some(6))),
        Some((Token::String(0, 6), Some(7))),
        Some((Token::String(0, 2), Some(3))),
        None,
        Some((Token::String(0, 7), Some(8))),
        Some((Token::String(0, 3), Some(4))),
    ],
    &[
        None,
        Some((Token::Regexp(1, 0), Some(3))),
        Some((Token::String(1, 0), Some(1))),
        None,
        Some((Token::String(1, 1), Some(2))),
    ],
];

const STATES: &[(usize, &[Action])] = &[
    (
        0,
        &[
            Reduce(4, 0),
            Reduce(4, 0),
            Error,
            Reduce(4, 0),
            Reduce(4, 0),
            Error,
            Error,
            Error,
            Reduce(4, 0),
            Reduce(4, 0),
        ],
    ),
    (
        0,
        &[
            Accept, Error, Error, Error, Error, Error, Error, Error, Error, Error,
        ],
    ),
    (
        0,
        &[
            Reduce(1, 0),
            Error,
            Reduce(1, 0),
            Error,
            Error,
            Error,
            Error,
            Error,
            Shift(5),
            Error,
        ],
    ),
    (
        0,
        &[
            Reduce(2, 0),
            Error,
            Reduce(2, 0),
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(2, 0),
            Error,
        ],
    ),
    (
        0,
        &[
            Reduce(3, 0),
            Shift(6),
            Reduce(3, 0),
            Shift(7),
            Shift(8),
            Error,
            Error,
            Error,
            Reduce(3, 0),
            Shift(9),
        ],
    ),
    (
        0,
        &[
            Reduce(4, 0),
            Reduce(4, 0),
            Reduce(4, 0),
            Reduce(4, 0),
            Reduce(4, 0),
            Error,
            Error,
            Error,
            Reduce(4, 0),
            Reduce(4, 0),
        ],
    ),
    (
        0,
        &[
            Error,
            Reduce(4, 0),
            Reduce(4, 0),
            Reduce(4, 0),
            Reduce(4, 0),
            Error,
            Error,
            Error,
            Reduce(4, 0),
            Reduce(4, 0),
        ],
    ),
    (1, &[Error, Shift(15), Error, Shift(16)]),
    (1, &[Error, Shift(15), Error, Shift(16)]),
    (
        0,
        &[
            Reduce(6, 0),
            Reduce(6, 0),
            Reduce(6, 0),
            Reduce(6, 0),
            Reduce(6, 0),
            Reduce(6, 0),
            Reduce(6, 0),
            Reduce(6, 0),
            Reduce(6, 0),
            Reduce(6, 0),
        ],
    ),
    (
        0,
        &[
            Reduce(4, 1),
            Reduce(4, 1),
            Reduce(4, 1),
            Reduce(4, 1),
            Reduce(4, 1),
            Error,
            Error,
            Error,
            Reduce(4, 1),
            Reduce(4, 1),
        ],
    ),
    (
        0,
        &[
            Reduce(5, 0),
            Reduce(5, 0),
            Reduce(5, 0),
            Reduce(5, 0),
            Reduce(5, 0),
            Shift(20),
            Shift(21),
            Shift(22),
            Reduce(5, 0),
            Reduce(5, 0),
        ],
    ),
    (
        0,
        &[
            Reduce(6, 1),
            Reduce(6, 1),
            Reduce(6, 1),
            Reduce(6, 1),
            Reduce(6, 1),
            Reduce(6, 1),
            Reduce(6, 1),
            Reduce(6, 1),
            Reduce(6, 1),
            Reduce(6, 1),
        ],
    ),
    (
        0,
        &[
            Reduce(2, 1),
            Error,
            Reduce(2, 1),
            Error,
            Error,
            Error,
            Error,
            Error,
            Reduce(2, 1),
            Error,
        ],
    ),
    (
        0,
        &[
            Error,
            Error,
            Shift(23),
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
            Error,
        ],
    ),
    (1, &[Error, Error, Reduce(8, 0), Reduce(8, 0)]),
    (1, &[Error, Shift(24), Reduce(9, 0), Reduce(9, 0)]),
    (1, &[Error, Error, Shift(25), Shift(16)]),
    (1, &[Error, Error, Reduce(8, 1), Reduce(8, 1)]),
    (1, &[Error, Error, Shift(27), Shift(16)]),
    (
        0,
        &[
            Reduce(5, 1),
            Reduce(5, 1),
            Reduce(5, 1),
            Reduce(5, 1),
            Reduce(5, 1),
            Error,
            Error,
            Error,
            Reduce(5, 1),
            Reduce(5, 1),
        ],
    ),
    (
        0,
        &[
            Reduce(5, 2),
            Reduce(5, 2),
            Reduce(5, 2),
            Reduce(5, 2),
            Reduce(5, 2),
            Error,
            Error,
            Error,
            Reduce(5, 2),
            Reduce(5, 2),
        ],
    ),
    (
        0,
        &[
            Reduce(5, 3),
            Reduce(5, 3),
            Reduce(5, 3),
            Reduce(5, 3),
            Reduce(5, 3),
            Error,
            Error,
            Error,
            Reduce(5, 3),
            Reduce(5, 3),
        ],
    ),
    (
        0,
        &[
            Reduce(6, 2),
            Reduce(6, 2),
            Reduce(6, 2),
            Reduce(6, 2),
            Reduce(6, 2),
            Reduce(6, 2),
            Reduce(6, 2),
            Reduce(6, 2),
            Reduce(6, 2),
            Reduce(6, 2),
        ],
    ),
    (1, &[Error, Error, Error, Shift(28)]),
    (
        0,
        &[
            Reduce(7, 0),
            Reduce(7, 0),
            Reduce(7, 0),
            Reduce(7, 0),
            Reduce(7, 0),
            Reduce(7, 0),
            Reduce(7, 0),
            Reduce(7, 0),
            Reduce(7, 0),
            Reduce(7, 0),
        ],
    ),
    (1, &[Error, Error, Reduce(8, 2), Reduce(8, 2)]),
    (
        0,
        &[
            Reduce(7, 1),
            Reduce(7, 1),
            Reduce(7, 1),
            Reduce(7, 1),
            Reduce(7, 1),
            Reduce(7, 1),
            Reduce(7, 1),
            Reduce(7, 1),
            Reduce(7, 1),
            Reduce(7, 1),
        ],
    ),
    (1, &[Error, Error, Reduce(9, 1), Reduce(9, 1)]),
];

fn goto(state: usize, nt: usize) -> usize {
    match (state, nt) {
        (0, 1) => 1,
        (0, 2) => 2,
        (0, 3) => 3,
        (0, 4) => 4,
        (4, 5) => 10,
        (4, 6) => 11,
        (4, 7) => 12,
        (5, 3) => 13,
        (5, 4) => 4,
        (6, 1) => 14,
        (6, 2) => 2,
        (6, 3) => 3,
        (6, 4) => 4,
        (7, 8) => 17,
        (7, 9) => 18,
        (8, 8) => 19,
        (8, 9) => 18,
        (17, 9) => 26,
        (19, 9) => 26,
        _ => unreachable!(),
    }
}

pub enum Symbol {
    /// Constant token
    T(R),
    /// `[ -[\]-~]|\\[ -~]`
    T0_0(Char),
    /// `[ -[\]-~]|\\[ -~]`
    T1_0(GroupChar),
    /// S
    S1(S),
    /// Or
    S2(Or),
    /// Concat
    S3(Concat),
    /// Sequence<Suffix>
    S4(Sequence<Suffix>),
    /// Suffix
    S5(Suffix),
    /// Unit
    S6(Unit),
    /// Group
    S7(Group),
    /// GroupContents
    S8(GroupContents),
    /// GroupMember
    S9(GroupMember),
}

impl SymbolExt<S, State> for Symbol {
    fn reduce(s: &mut State, s1: &mut Vec<Self>, s2: &mut Vec<usize>, nt: usize, rule: usize) {
        match (nt, rule) {
            (1, 0) => {
                s2.truncate(s2.len() - 1);
                let x = pop_many!(s1, S2);
                s1.push(S1(rule0_0(s, x.0)));
            }
            (2, 0) => {
                s2.truncate(s2.len() - 1);
                let x = pop_many!(s1, S3);
                s1.push(S2(rule1_0(s, x.0)));
            }
            (2, 1) => {
                s2.truncate(s2.len() - 3);
                let x = pop_many!(s1, S3, T, S2);
                s1.push(S2(rule1_1(s, x.2, x.1, x.0)));
            }
            (3, 0) => {
                s2.truncate(s2.len() - 1);
                let x = pop_many!(s1, S4);
                s1.push(S3(rule2_0(s, x.0)));
            }
            (4, 0) => {
                s1.push(S4(rule8_0(s)));
            }
            (4, 1) => {
                s2.truncate(s2.len() - 2);
                let x = pop_many!(s1, S5, S4);
                s1.push(S4(rule8_1(s, x.1, x.0)));
            }
            (5, 0) => {
                s2.truncate(s2.len() - 1);
                let x = pop_many!(s1, S6);
                s1.push(S5(rule3_0(s, x.0)));
            }
            (5, 1) => {
                s2.truncate(s2.len() - 2);
                let x = pop_many!(s1, T, S6);
                s1.push(S5(rule3_1(s, x.1, x.0)));
            }
            (5, 2) => {
                s2.truncate(s2.len() - 2);
                let x = pop_many!(s1, T, S6);
                s1.push(S5(rule3_2(s, x.1, x.0)));
            }
            (5, 3) => {
                s2.truncate(s2.len() - 2);
                let x = pop_many!(s1, T, S6);
                s1.push(S5(rule3_3(s, x.1, x.0)));
            }
            (6, 0) => {
                s2.truncate(s2.len() - 1);
                let x = pop_many!(s1, T0_0);
                s1.push(S6(rule4_0(s, x.0)));
            }
            (6, 1) => {
                s2.truncate(s2.len() - 1);
                let x = pop_many!(s1, S7);
                s1.push(S6(rule4_1(s, x.0)));
            }
            (6, 2) => {
                s2.truncate(s2.len() - 3);
                let x = pop_many!(s1, T, S1, T);
                s1.push(S6(rule4_2(s, x.2, x.1, x.0)));
            }
            (7, 0) => {
                s2.truncate(s2.len() - 3);
                let x = pop_many!(s1, T, S8, T);
                s1.push(S7(rule5_0(s, x.2, x.1, x.0)));
            }
            (7, 1) => {
                s2.truncate(s2.len() - 3);
                let x = pop_many!(s1, T, S8, T);
                s1.push(S7(rule5_1(s, x.2, x.1, x.0)));
            }
            (8, 0) => {
                s2.truncate(s2.len() - 1);
                let x = pop_many!(s1, T);
                s1.push(S8(rule6_0(s, x.0)));
            }
            (8, 1) => {
                s2.truncate(s2.len() - 1);
                let x = pop_many!(s1, S9);
                s1.push(S8(rule6_1(s, x.0)));
            }
            (8, 2) => {
                s2.truncate(s2.len() - 2);
                let x = pop_many!(s1, S9, S8);
                s1.push(S8(rule6_2(s, x.1, x.0)));
            }
            (9, 0) => {
                s2.truncate(s2.len() - 1);
                let x = pop_many!(s1, T1_0);
                s1.push(S9(rule7_0(s, x.0)));
            }
            (9, 1) => {
                s2.truncate(s2.len() - 3);
                let x = pop_many!(s1, T1_0, T, T1_0);
                s1.push(S9(rule7_1(s, x.2, x.1, x.0)));
            }
            _ => unreachable!(),
        }
    }

    fn reduce_regexp(
        s: &mut State,
        s1: &mut Vec<Self>,
        mode: usize,
        id: usize,
        text: String,
        range: R,
    ) {
        match (mode, id) {
            (0, 9) => s1.push(T0_0(regex0_0(s, range, text))),
            (1, 3) => s1.push(T1_0(regex1_0(s, range, text))),
            _ => s1.push(T(range)),
        }
    }

    fn get_s(self) -> Option<S> {
        match self {
            S1(s) => Some(s),
            _ => None,
        }
    }
}
