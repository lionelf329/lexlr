use crate::{Parse, RegexTree, ALPHABET};
use std::collections::BTreeSet;
use std::ops::RangeInclusive;

mod generated;
mod reducers;

impl RegexTree {
    /// Creates a [RegexTree] from a regular expression.
    pub fn from_regexp(regexp: &str) -> Result<Self, String> {
        regexp
            .chars()
            .filter(|&x| x != '\r')
            .parse(&mut (), &generated::DATA)
            .map_err(|range| regexp[range.range()].to_owned())
    }
}

fn parse_char(s: &str) -> char {
    match &s[..] {
        "\\t" => '\t',
        "\\n" => '\n',
        "\\r" => '\r',
        "\\u" => crate::lexer::UNICODE,
        _ => s.chars().last().unwrap(),
    }
}

fn build_group(ranges: Vec<RangeInclusive<char>>, invert: bool) -> RegexTree {
    let mut chars: BTreeSet<char> = BTreeSet::new();
    for range in ranges {
        for c in range {
            if ALPHABET.contains(&c) {
                chars.insert(c);
            }
        }
    }
    let chars = invert_set(chars, invert)
        .into_iter()
        .map(RegexTree::char)
        .collect::<Vec<_>>();
    assert!(!chars.is_empty());
    RegexTree::Or(chars)
}

/// Converts a [`BTreeSet<char>`] into a [`Vec<char>`], inverting it if necessary.
fn invert_set(chars: BTreeSet<char>, invert: bool) -> Vec<char> {
    if invert {
        let mut new_chars = Vec::with_capacity(ALPHABET.len() - chars.len());
        for c in ALPHABET {
            if !chars.contains(&c) {
                new_chars.push(c);
            }
        }
        new_chars
    } else {
        chars.into_iter().collect()
    }
}
