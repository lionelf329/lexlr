use crate::{Action, Grammar, Item, MapOfSet, SymbolID, Token};
use std::collections::{BTreeMap, BTreeSet};

impl Grammar {
    pub(crate) fn process_state<T: Copy + Ord + From<Token>>(
        &self,
        closure: &BTreeMap<Item, BTreeSet<T>>,
    ) -> (
        BTreeMap<T, BTreeSet<Action>>,
        BTreeMap<SymbolID, BTreeMap<Item, BTreeSet<T>>>,
    ) {
        let mut action = BTreeMap::new();
        let mut goto = BTreeMap::new();
        for (item, follow) in closure {
            // Match the next element in the rule, if there is one
            match self.next_element(*item) {
                Some(SymbolID::T(Token::EOF)) => {
                    // Attempting to shift EOF means parsing has completed
                    action.add(Token::EOF.into(), Action::Accept);
                }
                Some(next_element) => {
                    // Attempting to shift any other symbol creates a new item, which needs to
                    // be stored in a set
                    goto.entry(next_element)
                        .or_insert(BTreeMap::new())
                        .insert(item.next(), follow.clone());
                }
                None => {
                    // In there's nothing left in the rule, any valid following terminal should
                    // lead to a reduce action.
                    for &terminal in follow {
                        action.add(terminal, Action::Reduce(item.nt, item.r));
                    }
                }
            }
        }
        (action, goto)
    }
}
