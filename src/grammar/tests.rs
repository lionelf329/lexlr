use super::Grammar;
use crate::{
    GenericNT, Language, NonTerminal, RuleElement as RE, SymbolID::*, SymbolRef, Terminal,
    Token::*, Vocabulary,
};

#[test]
fn test_make_tables() {
    let language = Language {
        vocabularies: vec![Vocabulary {
            strings: vec![
                Terminal::new("+", "+", false),
                Terminal::new("*", "*", false),
            ],
            regexps: vec![
                Terminal::new("int", "[0-9]+", false),
                Terminal::new("id", "[_a-zA-Z][-a-zA-Z0-9]*", false),
            ],
        }],
        symbols: vec![
            GenericNT::new(
                "S",
                vec![],
                [
                    (0, vec![RE::new("S"), RE::new("+"), RE::new("P")]),
                    (1, vec![RE::new("P")]),
                ]
                .into(),
            ),
            GenericNT::new(
                "P",
                vec![],
                [
                    (0, vec![RE::new("P"), RE::new("*"), RE::new("V")]),
                    (1, vec![RE::new("V")]),
                ]
                .into(),
            ),
            GenericNT::new(
                "V",
                vec![],
                [(0, vec![RE::new("int")]), (1, vec![RE::new("id")])].into(),
            ),
        ],
    };
    let (language, errors) = language.resolve_refs();
    assert!(errors.no_errors());
    assert_eq!(
        language.nonterminals().unwrap(),
        Grammar(vec![
            NonTerminal {
                name: RE::new(SymbolRef::StartSymbol),
                rules: [(0, vec![NT(1), T(EOF)])].into(),
                epsilon: false,
                first: [Regexp(0, 0), Regexp(0, 1)].into(),
            },
            NonTerminal {
                name: RE::new(SymbolRef::Symbol(0)),
                rules: [(0, vec![NT(1), T(String(0, 0)), NT(2)]), (1, vec![NT(2)])].into(),
                epsilon: false,
                first: [Regexp(0, 0), Regexp(0, 1)].into(),
            },
            NonTerminal {
                name: RE::new(SymbolRef::Symbol(1)),
                rules: [(0, vec![NT(2), T(String(0, 1)), NT(3)]), (1, vec![NT(3)])].into(),
                epsilon: false,
                first: [Regexp(0, 0), Regexp(0, 1)].into(),
            },
            NonTerminal {
                name: RE::new(SymbolRef::Symbol(2)),
                rules: [(0, vec![T(Regexp(0, 0))]), (1, vec![T(Regexp(0, 1))])].into(),
                epsilon: false,
                first: [Regexp(0, 0), Regexp(0, 1)].into(),
            }
        ]),
    );
}
