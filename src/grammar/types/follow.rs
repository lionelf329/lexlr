use crate::{Item, Token};

/// A generic description for something in a follow set, which can either be a literal token, or
/// something inherited from the follow set of a core item.
#[derive(Copy, Clone, Debug, Ord, PartialOrd, Eq, PartialEq)]
pub enum Follow {
    /// An inherited follow set from a core item
    Item(Item),
    /// A specific token
    Token(Token),
}

impl Follow {
    /// Returns the inner [Item], if it is that variant.
    pub fn item(&self) -> Option<Item> {
        match self {
            Follow::Item(item) => Some(*item),
            Follow::Token(_) => None,
        }
    }

    /// Returns the inner [Token], if it is that variant.
    pub fn token(&self) -> Option<Token> {
        match self {
            Follow::Item(_) => None,
            Follow::Token(token) => Some(*token),
        }
    }
}

impl From<Token> for Follow {
    fn from(value: Token) -> Self {
        Self::Token(value)
    }
}
