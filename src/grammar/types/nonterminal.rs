use crate::{RuleElement, SymbolID, SymbolRef, Token};
use std::collections::{BTreeMap, BTreeSet};

/// A nonterminal symbol in a context free grammar, without generics.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct NonTerminal {
    /// The fully qualified name of the nonterminal, which can be used to find the original
    /// elements it was created from
    pub name: RuleElement<SymbolRef>,
    /// The list of rules, where each element in a rule is an 'address' of the symbol in the
    /// language
    pub rules: BTreeMap<usize, Vec<SymbolID>>,
    /// True if the nonterminal can be matched by empty input, otherwise false
    pub epsilon: bool,
    /// The set of terminals that can appear at the beginning of this nonterminal
    pub first: BTreeSet<Token>,
}

impl NonTerminal {
    /// Creates a new nonterminal from a name and a set of rules.
    ///
    /// By default, `epsilon` is false and the `first` and `follow` sets are empty.
    pub fn new(name: RuleElement<SymbolRef>, rules: BTreeMap<usize, Vec<SymbolID>>) -> Self {
        NonTerminal {
            name,
            rules,
            epsilon: false,
            first: Default::default(),
        }
    }
}

impl From<&RuleElement<SymbolRef>> for NonTerminal {
    fn from(name: &RuleElement<SymbolRef>) -> Self {
        NonTerminal {
            name: name.clone(),
            rules: BTreeMap::new(),
            epsilon: false,
            first: Default::default(),
        }
    }
}
