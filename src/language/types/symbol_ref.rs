use crate::Language;

/// An `enum` representing a location in the original language that a string points to.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub enum SymbolRef {
    /// A literal string, identified by a lexer mode and an index.
    String(usize, usize),
    /// A regular expression, identified by a lexer mode and an index.
    Regexp(usize, usize),
    /// A nonterminal, identified by an index.
    Symbol(usize),
    /// An unsubstituted generic parameter, identified by an index.
    Generic(usize),
    /// The new start symbol that is added when the grammar is augmented.
    StartSymbol,
}

impl SymbolRef {
    /// Returns `Some(usize)` if it was the right variant, otherwise `None`.
    pub fn symbol(&self) -> Option<usize> {
        match self {
            SymbolRef::Symbol(i) => Some(*i),
            _ => None,
        }
    }

    /// Returns a string describing the type of the rule element in the generated code.
    pub fn type_str<'a, T>(&self, generics: &'a [String], language: &'a Language<T>) -> &'a str {
        match *self {
            SymbolRef::String(_, _) => "R",
            SymbolRef::Regexp(v, i) => &language.vocabularies[v].regexps[i].name,
            SymbolRef::Symbol(i) => &language.symbols[i].name,
            SymbolRef::Generic(i) => &generics[i],
            SymbolRef::StartSymbol => panic!(),
        }
    }

    /// Returns a string describing the type of the rule element in the generated code.
    pub fn describe<'a, T>(&self, generics: &'a [String], language: &'a Language<T>) -> &'a str {
        match *self {
            SymbolRef::String(v, i) => &language.vocabularies[v].strings[i].name,
            SymbolRef::Regexp(v, i) => &language.vocabularies[v].regexps[i].name,
            SymbolRef::Symbol(i) => &language.symbols[i].name,
            SymbolRef::Generic(i) => &generics[i],
            SymbolRef::StartSymbol => "~",
        }
    }
}
