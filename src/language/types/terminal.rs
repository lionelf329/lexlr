/// A terminal symbol in a regular language.
#[derive(Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub struct Terminal {
    /// How it will be referred to in the rules in the context free grammar.
    pub name: String,
    /// The string literal or regular expression being matched.
    pub text: String,
    /// True if the lexer should filter out this token.
    pub ignored: bool,
}

impl Terminal {
    /// Creates a terminal from a name and text.
    ///
    /// This is just a convenience wrapper to avoid using `String::from(s)` or `s.to_string()`
    /// when constructing instances by hand.
    pub fn new(name: &str, text: &str, ignored: bool) -> Terminal {
        Terminal {
            name: String::from(name),
            text: String::from(text),
            ignored,
        }
    }
}
