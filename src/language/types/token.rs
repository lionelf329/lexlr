/// An identifier for a terminal in the grammar.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub enum Token {
    /// An identifier for a literal token, identified by a vocabulary index and an index inside
    /// that vocabulary
    String(usize, usize),
    /// An identifier for a regular expression, identified by a vocabulary index and an index inside
    /// that vocabulary
    Regexp(usize, usize),
    /// The EOF token, which is a special case that is needed for the LALR(1) algorithm
    EOF,
}
