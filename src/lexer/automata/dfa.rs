use crate::{char_to_index, Automata, Dfa, DfaState, Nfa, PartitionByKey, ALPHABET, ALPHABET_SIZE};
use std::collections::BTreeSet;

impl<T: Copy + Ord> Dfa<T> {
    /// Converts a nondeterministic finite state machine to a deterministic one, and minimizes it.
    pub fn new(nfa: &Nfa<T>) -> Self {
        let mut states: Vec<(BTreeSet<usize>, DfaState)> = Vec::new();
        states.push((nfa.start_state(), [None; ALPHABET_SIZE]));
        for i in 0.. {
            if i == states.len() {
                break;
            }
            for c in ALPHABET {
                if let Some(s) = nfa.transition(&states[i].0, c) {
                    if let Some(j) = states.iter().position(|(x, _)| x == &s) {
                        states[i].1[char_to_index(c)] = Some(j);
                    } else {
                        states[i].1[char_to_index(c)] = Some(states.len());
                        states.push((s, [None; ALPHABET_SIZE]));
                    }
                }
            }
        }
        let states: Vec<(Option<T>, DfaState)> = states
            .into_iter()
            .map(|(nfa_state, dfa_state)| (nfa.label(&nfa_state).copied(), dfa_state))
            .collect();

        let mut partitions: Vec<(usize, &DfaState)> = states
            .iter()
            .map(|(x, y)| (x, y))
            .partition_by_key()
            .collect();
        let mut count = partitions.iter().map(|(x, _)| *x).max().unwrap() + 1;
        loop {
            partitions = partitions
                .iter()
                .map(|&(p, s)| ((p, map_transitions(&partitions, s)), s))
                .partition_by_key()
                .collect();
            let new_count = partitions.iter().map(|(x, _)| *x).max().unwrap() + 1;
            if new_count != count {
                count = new_count;
            } else {
                break;
            }
        }
        let mut new_states = vec![(None, [None; ALPHABET_SIZE]); count];
        let mut x = 0;
        for i in 0..states.len() {
            let (p, s) = partitions[i];
            if p == x {
                new_states[p].0 = states[i].0;
                for i in 0..s.len() {
                    if let Some(s) = s[i] {
                        new_states[p].1[i] = Some(partitions[s].0);
                    }
                }
                x += 1;
            }
        }
        Dfa { states: new_states }
    }
}

fn map_transitions(partitions: &[(usize, &DfaState)], &s: &DfaState) -> Vec<Option<usize>> {
    s.iter().map(|&s| s.map(|s| partitions[s].0)).collect()
}
