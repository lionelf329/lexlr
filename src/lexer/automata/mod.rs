use crate::ALPHABET_SIZE;
use std::collections::{BTreeMap, BTreeSet};

mod dfa;
mod dfa_impl;
mod nfa;
mod nfa_impl;

#[cfg(test)]
mod test;

/// A trait for finite state automata.
pub trait Automata<State, Label> {
    /// Returns the initial state of the automata before any input has been processed.
    fn start_state(&self) -> State;

    /// Returns the next state after processing the specified character, or None for the dead state.
    fn transition(&self, state: &State, char: char) -> Option<State>;

    /// Returns the state's label, indicating which pattern was matched.
    fn label(&self, state: &State) -> Option<&Label>;
}

/// A deterministic finite state machine.
#[derive(Debug)]
pub struct Dfa<T> {
    /// The list of states, each with a label and a list of transitions.
    pub states: Vec<(Option<T>, DfaState)>,
}

/// A single state in a DFA, containing an optional transition for each input character.
pub type DfaState = [Option<usize>; ALPHABET_SIZE];

/// A nondeterministic finite state machine.
#[derive(Debug)]
pub struct Nfa<T> {
    states: Vec<(Option<T>, BTreeMap<Option<char>, BTreeSet<usize>>)>,
}
