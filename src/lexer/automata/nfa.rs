use crate::{MapOfSet, Nfa, RegexTree, RegexTree::*};
use std::collections::{BTreeMap, VecDeque};
use NfaModification::*;

#[derive(Copy, Clone, Debug)]
pub enum NfaModification {
    AcceptingState,
    IntermediateStates(usize, usize, usize),
    EpsilonTransition(usize, usize),
    CharTransition(usize, usize),
    RegexTransition(usize, usize),
    DropEdge(usize, usize),
    Pause,
}

impl<T: Copy> Nfa<T> {
    /// Creates an empty NFA.
    pub fn new() -> Self {
        Nfa {
            states: vec![(None, BTreeMap::new())],
        }
    }

    /// Adds a pattern to the NFA with the specified label as the accepting state.
    pub fn add(&mut self, label: T, tree: &RegexTree) -> Vec<NfaModification> {
        let end = self.add_state(Some(label));
        let mut actions = vec![AcceptingState];
        let mut queue: VecDeque<(usize, usize, &RegexTree)> = VecDeque::new();
        self.enqueue(0, end, tree, &mut actions, &mut queue);
        while let Some((s1, s2, tree)) = queue.pop_front() {
            actions.push(DropEdge(s1, s2));
            match tree {
                Epsilon | Character(_) => unreachable!(),
                Plus(tree) => {
                    let chain = self.add_between(s1, s2, 2, &mut actions);
                    self.enqueue(chain[0], chain[1], tree, &mut actions, &mut queue);
                    self.enqueue(chain[1], chain[1], tree, &mut actions, &mut queue);
                    self.add_epsilon(chain[1], chain[2], &mut actions);
                }
                Star(tree) => {
                    let chain = self.add_between(s1, s2, 2, &mut actions);
                    self.add_epsilon(chain[0], chain[1], &mut actions);
                    self.enqueue(chain[1], chain[1], tree, &mut actions, &mut queue);
                    self.add_epsilon(chain[1], chain[2], &mut actions);
                }
                Optional(tree) => {
                    self.add_epsilon(s1, s2, &mut actions);
                    self.enqueue(s1, s2, tree, &mut actions, &mut queue);
                }
                Concat(trees) => {
                    let chain = self.add_between(s1, s2, trees.len(), &mut actions);
                    for i in 0..trees.len() {
                        self.enqueue(chain[i], chain[i + 1], &trees[i], &mut actions, &mut queue);
                    }
                }
                Or(trees) => {
                    for t in trees {
                        self.enqueue(s1, s2, t, &mut actions, &mut queue);
                    }
                }
            }
            actions.push(Pause);
        }
        actions
    }

    fn add_state(&mut self, label: Option<T>) -> usize {
        self.states.push((label, BTreeMap::new()));
        self.states.len() - 1
    }

    fn add_between(
        &mut self,
        s1: usize,
        s2: usize,
        count: usize,
        actions: &mut Vec<NfaModification>,
    ) -> Vec<usize> {
        actions.push(IntermediateStates(s1, s2, count));
        let mut states: Vec<_> = (1..count).map(|_| self.add_state(None)).collect();
        states.insert(0, s1);
        states.push(s2);
        states
    }

    fn add_transition(&mut self, from: usize, to: usize, ch: Option<char>) {
        self.states[from].1.add(ch, to);
    }

    fn add_epsilon(&mut self, from: usize, to: usize, actions: &mut Vec<NfaModification>) {
        actions.push(EpsilonTransition(from, to));
        self.add_transition(from, to, None);
    }

    fn enqueue<'a>(
        &mut self,
        from: usize,
        to: usize,
        tree: &'a RegexTree,
        actions: &mut Vec<NfaModification>,
        queue: &mut VecDeque<(usize, usize, &'a RegexTree)>,
    ) {
        match tree {
            Epsilon => {
                self.add_epsilon(from, to, actions);
            }
            &Character(char) => {
                actions.push(CharTransition(from, to));
                self.add_transition(from, to, Some(char))
            }
            _ => {
                actions.push(RegexTransition(from, to));
                queue.push_back((from, to, tree))
            }
        }
    }
}
