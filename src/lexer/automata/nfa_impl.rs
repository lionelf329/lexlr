use crate::{Automata, Nfa};
use std::collections::{BTreeSet, VecDeque};

impl<T> Automata<BTreeSet<usize>, T> for Nfa<T> {
    fn start_state(&self) -> BTreeSet<usize> {
        self.closure(BTreeSet::from([0]))
    }

    fn transition(&self, state: &BTreeSet<usize>, char: char) -> Option<BTreeSet<usize>> {
        let next: BTreeSet<usize> = state
            .iter()
            .filter_map(|&s| self.states[s].1.get(&Some(char)))
            .flatten()
            .copied()
            .collect();
        if next.is_empty() {
            None
        } else {
            Some(self.closure(next))
        }
    }

    fn label(&self, state: &BTreeSet<usize>) -> Option<&T> {
        state.iter().find_map(|&s| self.states[s].0.as_ref())
    }
}

impl<T> Nfa<T> {
    fn closure(&self, mut states: BTreeSet<usize>) -> BTreeSet<usize> {
        let mut unhandled: VecDeque<_> = states.iter().copied().collect();
        while let Some(state) = unhandled.pop_front() {
            if let Some(next_states) = self.states[state].1.get(&None) {
                for s in next_states {
                    if !states.contains(s) {
                        states.insert(*s);
                        unhandled.push_back(*s);
                    }
                }
            }
        }
        states
    }
}
