use crate::{Automata, Dfa, Nfa, RegexTree};

fn transition<State, Label, T: Automata<State, Label>>(
    machine: &T,
    s: &mut Option<State>,
    c: char,
) {
    *s = machine.transition(s.as_ref().unwrap(), c);
}

fn label<'a, State, Label, T: Automata<State, Label>>(
    machine: &'a T,
    s: &Option<State>,
) -> Option<Option<&'a Label>> {
    s.as_ref().map(|s| machine.label(s))
}

fn test_literal<State, T: Automata<State, usize>>(machine: T) {
    let mut s = Some(machine.start_state());
    assert_eq!(label(&machine, &s), Some(None));
    transition(&machine, &mut s, 'a');
    assert_eq!(label(&machine, &s), Some(None));
    transition(&machine, &mut s, 'b');
    assert_eq!(label(&machine, &s), Some(None));
    transition(&machine, &mut s, 'c');
    assert_eq!(label(&machine, &s), Some(Some(&0)));
    transition(&machine, &mut s, 'd');
    assert_eq!(label(&machine, &s), None);
}

fn test_regex<State, T: Automata<State, usize>>(machine: T) {
    let mut s = Some(machine.start_state());
    assert_eq!(label(&machine, &s), Some(None));
    transition(&machine, &mut s, 'a');
    assert_eq!(label(&machine, &s), Some(Some(&0)));
    transition(&machine, &mut s, 'b');
    assert_eq!(label(&machine, &s), Some(Some(&0)));
    transition(&machine, &mut s, 'b');
    assert_eq!(label(&machine, &s), Some(Some(&0)));
    transition(&machine, &mut s, 'c');
    assert_eq!(label(&machine, &s), None);
}

#[test]
fn test_nfa() {
    let mut nfa: Nfa<usize> = Nfa::new();
    nfa.add(0, &RegexTree::from_string("abc").unwrap());
    test_literal(nfa);

    let mut nfa: Nfa<usize> = Nfa::new();
    nfa.add(0, &RegexTree::from_regexp("ab*").unwrap());
    test_regex(nfa);
}

#[test]
fn test_dfa() {
    let mut nfa: Nfa<usize> = Nfa::new();
    nfa.add(0, &RegexTree::from_string("abc").unwrap());
    test_literal(Dfa::new(&nfa));

    let mut nfa: Nfa<usize> = Nfa::new();
    nfa.add(0, &RegexTree::from_regexp("ab*").unwrap());
    test_regex(Dfa::new(&nfa));
}
