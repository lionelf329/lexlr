use crate::{Language, SymbolRef, Token, Vocabulary};
use std::fmt::Debug;

pub use automata::*;
pub use regex::*;

mod automata;
mod regex;

/// A lexer that allows for multiple modes for different languages.
#[derive(Debug)]
pub struct Lexer {
    /// The list of automata, one for each lexer mode.
    pub automata: Vec<Dfa<Token>>,
}

impl Lexer {
    /// Constructs a collection of deterministic finite state automata from a language definition.
    pub fn new(language: &Language<SymbolRef>) -> Result<Self, String> {
        let automata = language
            .vocabularies
            .iter()
            .enumerate()
            .map(|(i, vocabulary)| Self::make_dfa(i, vocabulary))
            .collect::<Result<Vec<Dfa<Token>>, String>>()?;
        Ok(Lexer { automata })
    }

    /// Constructs a single deterministic finite state automata from a vocabulary and its index.
    fn make_dfa(i: usize, vocabulary: &Vocabulary) -> Result<Dfa<Token>, String> {
        let mut nfa = Nfa::new();
        for (j, s) in vocabulary.strings.iter().enumerate() {
            nfa.add(Token::String(i, j), &RegexTree::from_string(&s.text)?);
        }
        for (j, r) in vocabulary.regexps.iter().enumerate() {
            nfa.add(Token::Regexp(i, j), &RegexTree::from_regexp(&r.text)?);
        }
        Ok(Dfa::new(&nfa))
    }

    /// Returns the state reached from a given previous state on a given lexer mode and input, or
    /// `None` if it is the dead state.
    pub fn transition(&self, mode: usize, state: usize, char: char) -> Option<usize> {
        self.automata[mode].transition(&state, char)
    }

    /// Returns the label associated with a given state index.
    pub fn label(&self, mode: usize, state: usize) -> Option<Token> {
        self.automata[mode].label(&state).copied()
    }
}
