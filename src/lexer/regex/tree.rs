use crate::ALPHABET;
use RegexTree::*;

/// A node of a parse tree for a regular expression.
#[derive(Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub enum RegexTree {
    /// The empty string
    Epsilon,
    /// A single character
    Character(char),
    /// A regex that can be repeated 1 or more times
    Plus(Box<RegexTree>),
    /// A regex that can be repeated 0 or more times
    Star(Box<RegexTree>),
    /// A regex that can optionally be skipped
    Optional(Box<RegexTree>),
    /// The concatenation of several regular expressions
    Concat(Vec<RegexTree>),
    /// A choice between several regular expressions
    Or(Vec<RegexTree>),
}

impl RegexTree {
    /// Creates a [RegexTree] from a string literal.
    pub fn from_string(string: &str) -> Result<Self, String> {
        string
            .chars()
            .map(|char| {
                if ALPHABET.contains(&char) {
                    Ok(Character(char))
                } else {
                    Err(format!("Invalid character in literal: {:?}", char))
                }
            })
            .collect::<Result<Vec<_>, _>>()
            .map(|mut chars| match chars.len() {
                0 => Epsilon,
                1 => chars.pop().unwrap(),
                _ => Concat(chars),
            })
    }

    /// Creates a [RegexTree] from a character.
    pub fn char(c: char) -> RegexTree {
        Character(c)
    }

    /// Creates a new [RegexTree] from an old [RegexTree], where the new one represents the old one
    /// being repeated one or more times.
    pub fn plus(t: RegexTree) -> RegexTree {
        Plus(Box::new(t))
    }

    /// Creates a new [RegexTree] from an old [RegexTree], where the new one represents the old one
    /// being repeated zero or more times.
    pub fn star(t: RegexTree) -> RegexTree {
        Star(Box::new(t))
    }

    /// Creates a new [RegexTree] from an old [RegexTree], where the new one accepts what the old
    /// one accepts, but also the empty string.
    pub fn optional(t: RegexTree) -> RegexTree {
        Optional(Box::new(t))
    }

    /// Creates a new [RegexTree] from two [RegexTree]s, where the new one accepts everything that
    /// either of the parameters accept.
    pub fn or(mut t1: RegexTree, mut t2: RegexTree) -> RegexTree {
        match (&mut t1, &mut t2) {
            (Or(t1_trees), Or(t2_trees)) => {
                t1_trees.append(t2_trees);
                t1
            }
            (Or(t1_trees), _) => {
                t1_trees.push(t2);
                t1
            }
            (_, Or(t2_trees)) => {
                t2_trees.insert(0, t1);
                t2
            }
            (_, _) => Or(vec![t1, t2]),
        }
    }
}
