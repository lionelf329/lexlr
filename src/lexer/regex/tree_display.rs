use crate::{RegexTree, RegexTree::*, UNICODE};
use std::fmt::{Display, Formatter, Result, Write};

#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
enum Precedence {
    None = 0,
    Concat = 1,
    Suffix = 2,
}

impl Display for RegexTree {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        re2s(self, f, Precedence::None)
    }
}

/// Recursively writes the regex tree using the formatter, adding parentheses where necessary.
fn re2s(tree: &RegexTree, f: &mut Formatter<'_>, nested_in: Precedence) -> Result {
    match tree {
        Epsilon => Ok(()),
        &Character(c) => write_escaped_char(f, c),
        Plus(tree) => {
            re2s(tree, f, Precedence::Suffix)?;
            f.write_char('+')
        }
        Star(tree) => {
            re2s(tree, f, Precedence::Suffix)?;
            f.write_char('*')
        }
        Optional(tree) => {
            re2s(tree, f, Precedence::Suffix)?;
            f.write_char('?')
        }
        Concat(trees) => {
            if nested_in > Precedence::Concat {
                f.write_char('(')?;
                for t in trees {
                    re2s(t, f, Precedence::Concat)?;
                }
                f.write_char(')')
            } else {
                for t in trees {
                    re2s(t, f, Precedence::Concat)?;
                }
                Ok(())
            }
        }
        Or(trees) => or(trees, f, nested_in > Precedence::None),
    }
}

/// Writes a character, escaping special characters as necessary.
fn write_escaped_char(f: &mut Formatter<'_>, c: char) -> Result {
    match c {
        UNICODE => f.write_str("\\u"),
        '\t' => f.write_str("\\t"),
        '\n' => f.write_str("\\n"),
        '\r' => f.write_str("\\r"),
        '(' | ')' | '[' | ']' | '*' | '+' | '?' | '|' => {
            f.write_char('\\')?;
            f.write_char(c)
        }
        _ => {
            assert!(' ' <= c && c <= '~');
            f.write_char(c)
        }
    }
}

/// Writes all the terms in an "or" statement, dealing with all the complexity that surrounds how
/// and when group constructs can be used to shorten the output.
fn or(trees: &[RegexTree], f: &mut Formatter<'_>, precedence_too_low: bool) -> Result {
    let (mut chars, other) = flatten(trees);
    chars.sort();
    chars.dedup();

    // Use parens when there is stuff that needs to be bracketed but can't be grouped
    let parens = precedence_too_low && !other.is_empty();

    // Use a group when it eliminates the need for brackets, or when there are enough characters
    let group = (precedence_too_low && other.is_empty()) || chars.len() > 2;

    if parens {
        f.write_char('(')?;
    }
    if group {
        let ranges = get_ranges(&chars);
        write_group(f, ranges)?;
    } else if chars.len() > 0 {
        write_escaped_char(f, chars[0])?;
        for &char in &chars[1..] {
            f.write_char('|')?;
            write_escaped_char(f, char)?;
        }
    }
    if !other.is_empty() {
        if !chars.is_empty() {
            f.write_char('|')?;
        }
        re2s(&other[0], f, Precedence::None)?;
        for o in &other[1..] {
            f.write_char('|')?;
            re2s(o, f, Precedence::None)?;
        }
        if parens {
            f.write_char(')')?;
        }
    }
    Ok(())
}

/// Writes a group using a formatter and a list of character ranges.
fn write_group(f: &mut Formatter<'_>, ranges: Vec<(char, char)>) -> Result {
    f.write_char('[')?;
    if ranges[0].0 == '^' {
        f.write_char('\\')?;
    }
    for (c1, c2) in ranges {
        write_escaped_char(f, c1)?;
        if c1 != c2 {
            f.write_char('-')?;
            write_escaped_char(f, c2)?;
        }
    }
    f.write_char(']')
}

/// Finds all the leaves in a regex tree, where only "or" operations are considered to be internal
/// nodes. It then gives a list of all the leaves that are individual characters (unwrapped to
/// expose the characters themselves), and a separate list of all other children.
fn flatten(children: &[RegexTree]) -> (Vec<char>, Vec<&RegexTree>) {
    let mut chars = Vec::new();
    let mut trees = Vec::new();
    for t in children {
        if let &Character(c) = t {
            chars.push(c);
        } else {
            trees.push(t);
        }
    }
    (chars, trees)
}

/// Takes a list of characters and gives a list of (start, end) tuples that describe the unbroken
/// consecutive sequences in it.
fn get_ranges(chars: &[char]) -> Vec<(char, char)> {
    let mut ranges = Vec::new();
    let mut start = 0;
    for i in 0..chars.len() {
        if chars[i] as u8 - chars[start] as u8 != (i - start) as u8 {
            add_range(&mut ranges, chars[start], chars[i - 1]);
            start = i;
        }
    }
    let last = *chars.last().unwrap();
    add_range(&mut ranges, chars[start], last);
    ranges
}

/// Takes a range and decides whether to add it directly or to split it up as two one-character
/// ranges. This is important because "x-y" should just be written as "xy".
fn add_range(ranges: &mut Vec<(char, char)>, c1: char, c2: char) {
    let width = c2 as u8 - c1 as u8;
    if width == 1 {
        ranges.push((c1, c1));
        ranges.push((c2, c2));
    } else {
        ranges.push((c1, c2));
    }
}

#[cfg(test)]
mod tests {
    use super::{add_range, flatten, get_ranges};
    use crate::{RegexTree as RT, RegexTree::*, UNICODE};

    #[test]
    fn test_string_parse() {
        test_string("abc", "abc");
        test_string("ab+", "ab\\+");
        test_string("[ab]", "\\[ab\\]");
        test_string("a|b", "a\\|b");
        test_string("a?b+c*", "a\\?b\\+c\\*");
        test_string("", "");
    }

    fn test_string(literal: &str, output: &str) {
        assert_eq!(&RT::from_string(literal).unwrap().to_string(), output)
    }

    #[test]
    fn test_regexp_parse() {
        test_regexp("abc", "abc");
        test_regexp("ab+", "ab+");
        test_regexp("ab\\+", "ab\\+");
        test_regexp("[ab]", "a|b");
        test_regexp("[^ -~]", "[\\t\\n\\r\\u]");
        test_regexp("a|b", "a|b");
        test_regexp("a?b+c*", "a?b+c*");
        test_regexp("(ab)?(cd)+(ef)*", "(ab)?(cd)+(ef)*");
        test_regexp("(ab|)(cd)+(ef)*", "(ab|)(cd)+(ef)*");
        test_regexp("", "");
    }

    fn test_regexp(regex: &str, output: &str) {
        assert_eq!(&RT::from_regexp(regex).unwrap().to_string(), output)
    }

    #[test]
    fn test_write_escaped_char() {
        assert_eq!(&RT::char('\t').to_string(), "\\t");
        assert_eq!(&RT::char('\n').to_string(), "\\n");
        assert_eq!(&RT::char('\r').to_string(), "\\r");
        assert_eq!(&RT::char(UNICODE).to_string(), "\\u");
        assert_eq!(&RT::char('(').to_string(), "\\(");
        assert_eq!(&RT::char(')').to_string(), "\\)");
        assert_eq!(&RT::char('[').to_string(), "\\[");
        assert_eq!(&RT::char(']').to_string(), "\\]");
        assert_eq!(&RT::char('*').to_string(), "\\*");
        assert_eq!(&RT::char('+').to_string(), "\\+");
        assert_eq!(&RT::char('?').to_string(), "\\?");
        assert_eq!(&RT::char('|').to_string(), "\\|");
        assert_eq!(&RT::char('x').to_string(), "x");
    }

    #[test]
    fn test_flatten() {
        if let Or(children) = &RT::or(
            RT::or(RT::char('a'), RT::char('b')),
            RT::star(RT::char('c')),
        ) {
            assert_eq!(
                flatten(children),
                (vec!['a', 'b'], vec![&RT::star(RT::char('c'))])
            );
        } else {
            panic!();
        }
        if let Or(children) = &RT::or(
            RT::or(RT::char('a'), RT::char('b')),
            RT::or(RT::star(RT::char('c')), RT::plus(RT::char('d'))),
        ) {
            assert_eq!(
                flatten(children),
                (
                    vec!['a', 'b'],
                    vec![&RT::star(RT::char('c')), &RT::plus(RT::char('d'))]
                )
            );
        } else {
            panic!();
        }
    }

    #[test]
    fn test_get_ranges() {
        assert_eq!(get_ranges(&vec!['a']), vec![('a', 'a')]);
        assert_eq!(get_ranges(&vec!['a', 'b']), vec![('a', 'a'), ('b', 'b')]);
        assert_eq!(get_ranges(&vec!['a', 'c']), vec![('a', 'a'), ('c', 'c')]);
        assert_eq!(get_ranges(&vec!['a', 'b', 'c']), vec![('a', 'c')]);
        assert_eq!(
            get_ranges(&vec!['a', 'b', 'c', 'e', 'f']),
            vec![('a', 'c'), ('e', 'e'), ('f', 'f')]
        );
        assert_eq!(
            get_ranges(&vec!['a', 'b', 'c', 'e', 'f', 'g']),
            vec![('a', 'c'), ('e', 'g')]
        );
    }

    #[test]
    fn test_add_range() {
        let mut ranges = Vec::new();
        add_range(&mut ranges, 'a', 'a');
        assert_eq!(ranges, vec![('a', 'a')]);
        ranges = Vec::new();
        add_range(&mut ranges, 'a', 'b');
        assert_eq!(ranges, vec![('a', 'a'), ('b', 'b')]);
        ranges = Vec::new();
        add_range(&mut ranges, 'a', 'c');
        assert_eq!(ranges, vec![('a', 'c')]);
    }
}
