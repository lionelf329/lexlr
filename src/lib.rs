//! lexlr is a tool for parsing context free languages whose vocabulary is a regular language.
//!
//! For regular languages, it uses a definition comprised of a set of literals, and a set of
//! regular expressions. Each of these has a name attached to it, which can be used in the context
//! free grammar. These sets are used to create a minimal deterministic finite state machine, which
//! enables tokenization in O(n) time and O(1) space.
//!
//! For context free languages, the definition is taken as a set of nonterminal definitions, each
//! with a set of rules, where each rule is a list of symbols. To reduce unnecessary repetition in
//! the grammar, generic parameters may be used in nonterminal definitions.

#![deny(unsafe_code)]
#![warn(
    absolute_paths_not_starting_with_crate,
    anonymous_parameters,
    explicit_outlives_requirements,
    missing_copy_implementations,
    missing_debug_implementations,
    missing_docs,
    noop_method_call,
    single_use_lifetimes,
    trivial_casts,
    trivial_numeric_casts,
    unused_import_braces,
    unused_lifetimes,
    unused_qualifications,
    variant_size_differences
)]

pub use algorithm::*;
pub use codegen::*;
pub use grammar::*;
pub use language::*;
pub use lexer::*;
pub use parser::*;
pub(crate) use utils::*;

mod algorithm;
mod codegen;
mod generated;
mod grammar;
mod language;
mod lexer;
mod parser;
mod utils;
