use crate::{color, Item, Parser, SymbolID, Token};
use std::collections::BTreeSet;
use std::fmt::{Display, Formatter, Result};

impl Display for Parser<'_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        f.write_str("-   ")?;
        for v in &self.language.vocabularies {
            for s in &v.strings {
                f.write_str(&format!("{:15}", &s.name))?;
            }
        }
        for v in &self.language.vocabularies {
            for s in &v.regexps {
                f.write_str(&format!("{:15}", &s.name))?;
            }
        }
        write!(f, "{:15}", "EOF")?;
        for s in &self.nonterminals {
            f.write_str(&format!("{:15}", &s.name.describe(&[], self.language)))?;
        }
        f.write_str("\n")?;
        for (i, s) in self.states.iter().enumerate() {
            f.write_str(&format!("{:<4}", i))?;
            for x in &s.action_string {
                for x in x {
                    f.write_str(&format!("{:15}", &format!("{:?}", x)))?;
                }
            }
            for x in &s.action_regexp {
                for x in x {
                    f.write_str(&format!("{:15}", &format!("{:?}", x)))?;
                }
            }
            f.write_str(&format!("{:15}", &format!("{:?}", s.action_eof)))?;
            for x in &s.goto {
                f.write_str(&format!("{:15}", &format!("{:?}", x)))?;
            }
            f.write_str("\n")?;
        }
        Ok(())
    }
}

impl Parser<'_> {
    /// Converts an item to a string for display purposes
    pub fn item_to_str(&self, item: Item) -> String {
        let nt = &self.nonterminals[item.nt];
        let rule = &nt.rules[&item.r];
        let mut item_elements: Vec<String> = rule
            .iter()
            .map(|&x| match x {
                SymbolID::T(x) => color(self.language.label(x), 34),
                SymbolID::NT(x) => {
                    color(&self.nonterminals[x].name.describe(&[], self.language), 35)
                }
            })
            .collect();
        item_elements.insert(item.i, color("⬤", 32));
        format!(
            "{}  →  {}",
            color(&nt.name.describe(&[], self.language), 35),
            item_elements.join("  ")
        )
    }

    /// Converts a set of tokens to a string for display purposes
    pub fn tokens_to_str(&self, tokens: &BTreeSet<Token>) -> String {
        let mut follow_elements: Vec<String> = tokens
            .iter()
            .map(|&x| self.language.label(x).to_string())
            .collect();
        follow_elements.sort();
        format!("{{{}}}", follow_elements.join("  "))
    }
}
