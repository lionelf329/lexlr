use crate::{Grammar, InsertMany, KeySet, Language, StateInfo, SymbolID, SymbolRef, Token};
use std::collections::{BTreeMap, BTreeSet};

pub use types::{Action, Item, State};

mod impl_display;
mod types;

#[cfg(test)]
mod tests;

/// A set of LALR(1) parse tables.
#[derive(Debug)]
pub struct Parser<'a> {
    /// The language used to construct the table.
    pub language: &'a Language<SymbolRef>,
    /// The concrete nonterminals, with generics accounted for.
    pub nonterminals: Grammar,
    /// The lalr states, with metadata for determining which canonical LR states can be merged.
    pub combined_states: BTreeMap<BTreeSet<Item>, StateInfo>,
    /// The states in the LALR parse tables.
    pub states: Vec<State>,
}

impl<'a> Parser<'a> {
    /// Creates a LALR(1) parse table from a language
    pub fn new(language: &'a Language<SymbolRef>) -> Self {
        let nonterminals = language.nonterminals().unwrap();
        let combined_states = nonterminals.combined_states().unwrap();
        let mut table = Parser {
            language,
            nonterminals,
            combined_states,
            states: Vec::new(),
        };

        let mut to_process = BTreeSet::new();

        table.find_state(
            &mut to_process,
            BTreeMap::from([(Item::new(0, 0), BTreeSet::new())]),
        );

        // While there are states that need to be looked at, take the first one
        while let Some(current_state) = to_process.pop_first() {
            // Get the closure of the items in the current state
            let closure = table
                .nonterminals
                .closure(&table.states[current_state].items);

            let (action, goto) = table.nonterminals.process_state(&closure);

            for (token, action) in action {
                for action in action {
                    table.add_action(current_state, token, action);
                }
            }
            // For each symbol that might be shifted, create a state for the new item set, and add
            // the appropriate transitions from the current state to that new state. (note that
            // it will not always create new states, it can loop back)
            for (id, items) in goto {
                let next_state = table.find_state(&mut to_process, items);
                table.add_transition(current_state, next_state, id);
            }
        }

        table
    }

    /// Set what happens when you are in the state with id `state_id` and have `t` as a lookahead
    fn add_action(&mut self, state_id: usize, t: Token, action: Action) {
        if let Token::Regexp(lexer, _) | Token::String(lexer, _) = t {
            match &mut self.states[state_id].lexer {
                Some(current) => {
                    if *current != lexer {
                        println!(
                            "Conflict in parse table at state {}, multiple lexers needed",
                            state_id
                        )
                    }
                }
                x => *x = Some(lexer),
            };
        }
        let a = self.states[state_id].action_mut(t);
        if let Action::Error = a {
            *a = action;
        } else if action != *a {
            println!(
                "Conflict in parse table at state {} on terminal {}\n- {:?}\n- {:?}",
                state_id,
                self.language.label(t),
                a,
                action
            );
        }
    }

    /// Set what happens when you "shift" a symbol, which could be a terminal or a nonterminal.
    /// If it is a terminal, it appears as a Shift entry in the action table, if it is a
    /// nonterminal, it appears in the goto table.
    fn add_transition(&mut self, current_state: usize, next_state: usize, symbol: SymbolID) {
        match symbol {
            SymbolID::T(t) => self.add_action(current_state, t, Action::Shift(next_state)),
            SymbolID::NT(nt) => self.states[current_state].goto[nt] = Some(next_state),
        }
    }

    /// Get the index for a state with the given items, creating it if it doesn't already exist.
    /// If it does exist, update it with the given follow sets. If anything was changed, enqueue
    /// the new state.
    fn find_state(
        &mut self,
        to_process: &mut BTreeSet<usize>,
        items: BTreeMap<Item, BTreeSet<Token>>,
    ) -> usize {
        let candidates = self
            .states
            .iter()
            .enumerate()
            .filter(|(_, s)| self.is_compatible(&items, &s.items))
            .map(|(i, _)| i)
            .collect::<Vec<_>>();
        if candidates.len() > 1 {
            println!("There are multiple options for how to merge states");
        }
        // If there is an existing state with the same item set
        if let Some(index) = candidates.first().copied() {
            let state = &mut self.states[index];
            if update_follow_sets(&mut state.items, &items) {
                to_process.insert(index);
            }
            index
        } else {
            let index = self.states.len();
            self.states.push(State::new(
                items,
                &self.language.vocabularies,
                self.nonterminals.len(),
            ));
            to_process.insert(index);
            index
        }
    }

    fn is_compatible(
        &self,
        i1: &BTreeMap<Item, BTreeSet<Token>>,
        i2: &BTreeMap<Item, BTreeSet<Token>>,
    ) -> bool {
        i1.keys().eq(i2.keys())
            && self.combined_states[&i1.key_set()]
                .follow_exclusions
                .iter()
                .copied()
                .all(|[a, b]| {
                    let a = i1[&a].union(&i2[&a]).collect::<BTreeSet<_>>();
                    let b = i1[&b].union(&i2[&b]).collect::<BTreeSet<_>>();
                    a.is_disjoint(&b)
                })
    }
}

/// Update a list of follow sets with another list of follow sets, returning true if anything
/// was changed.
fn update_follow_sets(
    existing: &mut BTreeMap<Item, BTreeSet<Token>>,
    new: &BTreeMap<Item, BTreeSet<Token>>,
) -> bool {
    Iterator::zip(existing.iter_mut(), new.iter())
        .map(|((_, existing_item), (_, new_item))| existing_item.insert_many(new_item))
        .filter(|&x| x)
        .count()
        != 0
}
