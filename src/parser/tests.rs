use crate::{
    GenericNT, Language, OwnedParserData, ParseWrapper, RuleElement as RE, Terminal, Vocabulary,
};

#[test]
fn test_parse() {
    let language = Language {
        vocabularies: vec![Vocabulary {
            strings: vec![
                Terminal::new("+", "+", false),
                Terminal::new("*", "*", false),
            ],
            regexps: vec![
                Terminal::new("int", "[0-9]+", false),
                Terminal::new("id", "[_a-zA-Z][-a-zA-Z0-9]*", false),
            ],
        }],
        symbols: vec![
            GenericNT::new(
                "S",
                vec![],
                [
                    (0, vec![RE::new("S"), RE::new("+"), RE::new("P")]),
                    (1, vec![RE::new("P")]),
                ]
                .into(),
            ),
            GenericNT::new(
                "P",
                vec![],
                [
                    (0, vec![RE::new("P"), RE::new("*"), RE::new("V")]),
                    (1, vec![RE::new("V")]),
                ]
                .into(),
            ),
            GenericNT::new(
                "V",
                vec![],
                [(0, vec![RE::new("int")]), (1, vec![RE::new("id")])].into(),
            ),
        ],
    };
    let (language, errors) = language.resolve_refs();
    assert!(errors.no_errors());
    let data = OwnedParserData::new(&language);
    let input = "1+2*3+4";
    let parse = input.chars().parse(&data).unwrap();
    assert_eq!(format!("{:?}", parse), "S(S(1, +, P(2, *, 3)), +, 4)");
}
