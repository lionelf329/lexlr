/// An action to carry out based on the current parser state and the lookahead token.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub enum Action {
    /// Finish parsing, and return the only item on the stack.
    Accept,
    /// Shift the terminal onto the stack and go to the specified state.
    Shift(usize),
    /// Apply the reducer for the specified nonterminal and rule, then use the goto table to find
    /// the next state.
    Reduce(usize, usize),
    /// Reject the input because the lookahead token is wrong.
    Error,
}
