use crate::{Action, Item, Token, Vocabulary};
use std::collections::{BTreeMap, BTreeSet};

/// A state in the parser.
#[derive(Debug)]
pub struct State {
    /// The lexer mode to use when finding the lookahead token.
    pub lexer: Option<usize>,
    /// The set of items being parsed in this state.
    pub items: BTreeMap<Item, BTreeSet<Token>>,
    /// The segment of the action table for tokens that were specified as string literals.
    pub action_string: Vec<Vec<Action>>,
    /// The segment of the action table for tokens that were specified as regular expressions.
    pub action_regexp: Vec<Vec<Action>>,
    /// The segment of the action table for the special EOF token.
    pub action_eof: Action,
    /// The goto table, indicating what state to move to after shifting a nonterminal.
    pub goto: Vec<Option<usize>>,
}

impl State {
    /// Creates a new state with empty action and goto tables.
    ///
    /// The `vocabulary` and `symbols` parameters are needed to make the tables the correct size.
    pub fn new(
        items: BTreeMap<Item, BTreeSet<Token>>,
        vocabulary: &[Vocabulary],
        symbols: usize,
    ) -> Self {
        let action_string = vocabulary
            .iter()
            .map(|v| vec![Action::Error; v.strings.len()])
            .collect();
        let action_regexp = vocabulary
            .iter()
            .map(|v| vec![Action::Error; v.regexps.len()])
            .collect();
        State {
            lexer: None,
            items,
            action_string,
            action_regexp,
            action_eof: Action::Error,
            goto: vec![None; symbols],
        }
    }

    /// Gets the action for the specified token.
    pub fn action(&self, token: Token) -> Action {
        match token {
            Token::String(i, j) => self.action_string[i][j],
            Token::Regexp(i, j) => self.action_regexp[i][j],
            Token::EOF => self.action_eof,
        }
    }

    /// Gets a mutable reference to the action for the specified token.
    pub fn action_mut(&mut self, token: Token) -> &mut Action {
        match token {
            Token::String(i, j) => &mut self.action_string[i][j],
            Token::Regexp(i, j) => &mut self.action_regexp[i][j],
            Token::EOF => &mut self.action_eof,
        }
    }
}
