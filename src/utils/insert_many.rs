use std::collections::BTreeSet;

/// Adds an extra method on sets for inserting many items while still returning a value to
/// indicate whether anything was changed.
pub trait InsertMany<'a, Item: 'a + Copy> {
    fn insert_many<T: IntoIterator<Item = &'a Item>>(&mut self, other: T) -> bool;
}

impl<'a, Item: 'a + Copy + Ord> InsertMany<'a, Item> for BTreeSet<Item> {
    fn insert_many<T: IntoIterator<Item = &'a Item>>(&mut self, other: T) -> bool {
        other.into_iter().filter(|&&t| self.insert(t)).count() > 0
    }
}
