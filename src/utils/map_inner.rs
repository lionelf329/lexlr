use std::collections::BTreeMap;

/// Allows nested data structures to be mapped and re-collected.
pub trait MapInner<T, U> {
    type Result;
    /// Returns a nested vector computed by mapping each element using the given function.
    fn map_inner<F: FnMut(&T) -> U>(&self, f: F) -> Self::Result;
}

impl<T, U> MapInner<T, U> for Vec<Vec<T>> {
    type Result = Vec<Vec<U>>;

    fn map_inner<F: FnMut(&T) -> U>(&self, mut f: F) -> Self::Result {
        self.iter()
            .map(|x| x.iter().map(&mut f).collect())
            .collect()
    }
}

impl<K: Copy + Ord, T, U> MapInner<T, U> for BTreeMap<K, Vec<T>> {
    type Result = BTreeMap<K, Vec<U>>;

    fn map_inner<F: FnMut(&T) -> U>(&self, mut f: F) -> Self::Result {
        self.iter()
            .map(|(&k, x)| (k, x.iter().map(&mut f).collect()))
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use super::MapInner;

    #[test]
    fn test_map_inner() {
        assert_eq!(
            vec![vec![1, 2], vec![3, 4]].map_inner(|x| 2 * x),
            vec![vec![2, 4], vec![6, 8]]
        );
    }
}
