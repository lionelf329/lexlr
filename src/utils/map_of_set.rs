use std::collections::{BTreeMap, BTreeSet};

/// Splits a map into two maps based on whether each key has exactly one value or not.
pub trait MapOfSet<K, V> {
    fn add(&mut self, key: K, value: V) -> bool;

    fn one_to_one(self) -> (BTreeMap<K, V>, BTreeMap<K, BTreeSet<V>>);
}

impl<K: Ord, V: Ord> MapOfSet<K, V> for BTreeMap<K, BTreeSet<V>> {
    fn add(&mut self, key: K, value: V) -> bool {
        self.entry(key).or_insert(BTreeSet::new()).insert(value)
    }

    fn one_to_one(self) -> (BTreeMap<K, V>, BTreeMap<K, BTreeSet<V>>) {
        let mut ans1 = BTreeMap::new();
        let mut ans2 = BTreeMap::new();
        for (k, v) in self {
            if v.len() == 1 {
                ans1.insert(k, v.into_iter().next().unwrap());
            } else {
                ans2.insert(k, v);
            }
        }
        (ans1, ans2)
    }
}
