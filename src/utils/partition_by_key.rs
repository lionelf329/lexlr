use std::collections::BTreeMap;

/// Extension that remaps tuples of key-value pairs to use consecutive increasing integers as keys.
pub trait PartitionByKey<K: Ord, I> {
    /// Returns an iterator that remaps keys of arbitrary types to `usize` values.
    fn partition_by_key(self) -> Partition<I, K>;
}

impl<K: Ord, V, I: Iterator<Item = (K, V)>> PartitionByKey<K, I> for I {
    fn partition_by_key(self) -> Partition<I, K> {
        Partition {
            iter: self,
            map: BTreeMap::new(),
        }
    }
}

/// This iterator takes tuples of type (K, V), and turns them into tuples of type (usize, V), such
/// that if two elements have the same value K, then the assigned usize will also be equal. The
/// assigned keys will start at 0, and be used in order without gaps.
pub struct Partition<I, K: Ord> {
    iter: I,
    map: BTreeMap<K, usize>,
}

impl<I: Iterator<Item = (K, V)>, K: Ord, V> Iterator for Partition<I, K> {
    type Item = (usize, V);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|(k, v)| {
            let len = self.map.len();
            (*self.map.entry(k).or_insert(len), v)
        })
    }
}

#[cfg(test)]
mod tests {
    use super::PartitionByKey;

    #[test]
    fn test_partition_by_key() {
        test(
            vec![('a', 1), ('b', 2), ('a', 3), ('b', 4)],
            vec![(0, 1), (1, 2), (0, 3), (1, 4)],
        );
        test(
            vec![(-1, 1), (-2, 2), (-4, 3), (-3, 4)],
            vec![(0, 1), (1, 2), (2, 3), (3, 4)],
        );
        test(
            vec![('a', 0), ('b', 0), ('a', 0), ('c', 0)],
            vec![(0, 0), (1, 0), (0, 0), (2, 0)],
        );
    }

    fn test<K: Ord, V: Eq + std::fmt::Debug>(input: Vec<(K, V)>, output: Vec<(usize, V)>) {
        assert_eq!(
            input.into_iter().partition_by_key().collect::<Vec<_>>(),
            output
        );
    }
}
